from nltk.corpus import stopwords

class device:

    def __init__(self, name):
        self.final = False
        self.name = name
        self.options = []
        self.devices = []
        self.banned_words =  list(stopwords.words('english'))
        self.banned_words.insert(0,'anything')
        self.banned_words.append = ['it', 'I', 'me']
        self.banned_words.remove('on')
        self.banned_words.remove('off')
        self.banned_words.remove('with')
        self.banned_words.remove('without')

    def set_final(self, final):
        self.final = final

    def add_device(self, device):
        self.devices.insert(0, device)

    def set_devices(self, devices):
        self.devices = devices

    def get_devices(self):
        return self.devices

    def get_name(self):
        return self.name

    def check(self, model, sentense, sent_file = None, word_file = None):
        if len(self.devices) == 0:
            return ''
        if sent_file is not None:
            sent_f = open(sent_file, 'a')
            sent_f.write(sentense)

        if word_file is not None:
            word_f = open(word_file, 'a')
            word_f.write(f"********************\n{sentense}\n words:  \n")

        word_list = sentense.split()

        if (self.final):
            global_answer = ""
            print(self.name + " ")
            for device in self.devices:
                for option in device.get_devices():
                    word = option.get_name()
                    option.max = -1000
                    option.avg = 0
                    for i in range(len(word_list)):
                        if word_list[i] in self.banned_words:
                            continue
                        try:
                            sim = model.similarity(word, word_list[i])
                            if sim > option.max:
                                option.max_word = word_list[i]
                                option.max = sim
                                option.avg += sim
                            answer = f"{word} sim {word_list[i]} -> {str(sim)}"

                            if word_file is not None:
                                word_f.write(f"{answer}\n")
                        except Exception as err:
                            print(err)
                    option.avg /= len(word_list)
                device.get_devices().sort(key=lambda x: 2 * x.max + x.avg, reverse=True)
                global_answer += f" {device.get_name()} {device.get_devices()[0].name} "
            return global_answer


        for device in self.devices:
            word = device.get_name()

            device.max = -1000
            device.avg = 0
            for i in range(len(word_list)):
                if word_list[i] in self.banned_words:
                    continue
                try:
                    sim = model.similarity(word, word_list[i])
                    if sim > device.max:
                        device.max_word = word_list[i]
                        device.max = sim
                    device.avg += sim
                    answer = f"{word} sim {word_list[i]} -> {str(sim)}"

                    if word_file is not None:
                        word_f.write(f"{answer}\n")
                except Exception as err:
                    print(err)
            device.avg /= len(word_list)

            if sent_file is not None:
                # sent_f.write(f"the most similar {device.max_word} = {str(device.max)}\n")
                # sent_f.write(f"avg similar {str(device.avg)}\n")
                sent_f.write(device.max)
                sent_f.write(device.avg)

        if sent_file is not None:
            sent_f.close()
        if word_file is not None:
            word_f.close()

        self.devices.sort(key=lambda x: 2*x.max + x.avg, reverse=True)

        return self.devices[0].get_name() + self.devices[0].check(model, sentense, 'output/sent.txt', 'output/words.txt')