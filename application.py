from functors import *
from check_utils import SentenceCheckUtils
from device import Device
import nltk

nltk.download('stopwords')

SentenceCheckUtils.init_model('tmp/GoogleNews-vectors-negative300.bin')
coffee = Device(["coffee"])
lighting = Device(["lighting", "dark"])
music = Device(["music"])
TV = Device(["TV"])
air = Device(["air", "condition", "cold", "hot"])

coffee_1 = Device(["one"], HttpSender("172.168.0.1", "coffee one"))
coffee_2 = Device(["two"], HttpSender("172.168.0.1", "coffee two"))
coffee_3 = Device(["three"], HttpSender("172.168.0.1", "coffee three"))
coffee_4 = Device(["without", "no"], HttpSender("172.168.0.1", "coffee zero"))
coffee_5 = Device(["with"], HttpSender("172.168.0.1", "coffee one"))

lighting_1 = Device(["room"])
lighting_2 = Device(["kitchen"])
lighting_3 = Device(["bathroom"])
lighting_4 = Device(["hallway"])

music_1 = Device(["on"], TcpSender(1, "music on"))
music_2 = Device(["off"], TcpSender(1, "music off"))
music_3 = Device(["louder"], TcpSender(1, "music louder"))
music_4 = Device(["quieter"], TcpSender(1, "music quieter"))
music_5 = Device(["next"], TcpSender(1, "music next"))

TV_1 = Device(["on"], TcpSender(1, "TV on"))
TV_2 = Device(["off"], TcpSender(1, "TV off"))
TV_3 = Device(["louder"], TcpSender(1, "TV louder"))
TV_4 = Device(["quieter"], TcpSender(1, "TV quieter"))
TV_5 = Device(["next"], TcpSender(1, "TV next"))
TV_6 = Device(["previous"], TcpSender(1, "TV prev"))
TV_7 = Device(["channel"], TcpSender(1, "TV channel"))


air_1 = Device(["on"], TcpSender(1, "air on"))
air_2 = Device(["off"], TcpSender(1, "air off"))
air_3 = Device(["warmer"], TcpSender(1, "air warmer"))
air_4 = Device(["colder"], TcpSender(1, "air colder"))

lighting_1_1 = Device(["on", "dark"], HttpSender("172.168.0.2", "lighting room on"))
lighting_1_2 = Device(["off"], HttpSender("172.168.0.2", "lighting room off"))
lighting_2_1 = Device(["on", "dark"], HttpSender("172.168.0.2", "lighting kitchen on"))
lighting_2_2 = Device(["off"], HttpSender("172.168.0.2", "lighting kitchen off"))
lighting_3_1 = Device(["on", "dark"], HttpSender("172.168.0.2", "lighting bathroom on"))
lighting_3_2 = Device(["off"], HttpSender("172.168.0.2", "bathroom off"))
lighting_4_1 = Device(["on", "dark"], HttpSender("172.168.0.2", "lighting hallway on"))
lighting_4_2 = Device(["off"], HttpSender("172.168.0.2", "lighting hallway off"))

lighting_1_1.function()

home = Device(["home"])
home.set_devices([coffee,lighting,music,TV,air])

coffee.set_devices([coffee_1,coffee_2,coffee_3,coffee_4,coffee_5])

lighting_1.set_devices([lighting_1_1, lighting_1_2])
lighting_2.set_devices([lighting_2_1, lighting_2_2])
lighting_3.set_devices([lighting_3_1, lighting_3_2])
lighting_4.set_devices([lighting_4_1, lighting_4_2])
lighting.set_devices([lighting_1, lighting_2,lighting_3,lighting_4])

music.set_devices([music_1,music_2,music_3,music_4,music_5])
TV.set_devices([TV_1,TV_2,TV_3,TV_4,TV_5,TV_6])
air.set_devices([air_1,air_2,air_3,air_4])

input_file = open('input/cool_tests.txt', 'r')
result_f = open('output/result.txt', 'w')
result_s = open('output/result_1.txt', 'w')
for sentense in input_file:
    try:
        print("\n\n")
        print(f"sentense: {sentense}")
        answer = home.check(sentense, result_s)
        result_f.write(f"********************\n{sentense}\n")
        result_f.write(f"{answer}\n")
        answer.function()
    except Exception as errr:
        print("input_error")



input_file.close()
result_f.close()

while(True):
    sentense = input()
    if (sentense == 'exit'):
        break
    try:
        answer = home.check(sentense)
        print(answer)
        answer.function()
    except Exception as errr:
        print("input_error")



