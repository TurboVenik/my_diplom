import home_object

options = ['coffe', 'lamp', 'music', 'TV', 'heater']
home = home_object.device('home')

devices = []
device = home_object.device('coffee')
devices.insert(0, device)

device = home_object.device('lamp')
devices.insert(0, device)

device = home_object.device('music')
devices.insert(0, device)

device = home_object.device('TV')
devices.insert(0, device)

device = home_object.device('heater')
devices.insert(0, device)

home.set_devices(devices)
