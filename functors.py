from abc import abstractmethod

import requests


class Callable:

    @abstractmethod
    def __call__(self):
        raise NotImplementedError()

class HttpSender(Callable):

    def __init__(self, URL, body):
        self.URL = URL
        self.body = body

    def __call__(self):
        print(f"http request to '{self.URL}' with body '{self.body}'")
        # r = requests.post( self.URL, data= self.body)
        print(self.body)
       

class TcpSender(Callable):

    def __init__(self, socket_id, message):
        self.socket_id = socket_id
        self.message = message

    def __call__(self):
        print(f"send TCP message to socket '{self.socket_id}' with body '{self.message}'")