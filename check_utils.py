from nltk.corpus import stopwords

import pandas as pd
import sklearn.linear_model as lm

import gensim

class SentenceCheckUtils:

    __model = None
    __banned_words = None
    __coef = [1.0342585, 0.53217534]

    @staticmethod
    def set_coef(coef):
        SentenceCheckUtils.__coef = coef

    @staticmethod
    def create_dataset(input_file, output_file):
        output_file.write(",x1,x2,y\n")
        i = 0
        for sentense in input_file:
            try:
                split = sentense.split(";")
                SentenceCheckUtils.add_to_dataset(split[0], split[1], output_file, i, split[2])
                i = i + 1
            except Exception as err:
                print(err)

    @staticmethod
    def train(file):

        df = pd.DataFrame.from_csv(file)
        x = df.iloc[:, :-1]
        y = df.iloc[:, -1]
        skm = lm.LinearRegression()
        skm.fit(x, y)
        return skm.coef_

    @staticmethod
    def init_model(file):
        SentenceCheckUtils.__banned_words = set(stopwords.words('english'))
        SentenceCheckUtils.__banned_words.add('anything')
        SentenceCheckUtils.__banned_words.add('it')
        SentenceCheckUtils.__banned_words.add('I')
        SentenceCheckUtils.__banned_words.add('me')
        SentenceCheckUtils.__banned_words.remove('on')
        SentenceCheckUtils.__banned_words.remove('off')
        SentenceCheckUtils.__banned_words.remove('with')
        print('Loading of the model started...')
        SentenceCheckUtils.__model = gensim.models.KeyedVectors.load_word2vec_format(file, binary=True)
        print('Model loaded...')

    @staticmethod
    def add_to_dataset(sentense, name, data_f, index, f):
        word_list = sentense.split()
        max = -10000
        avg = 0
        for i in range(len(word_list)):
            if word_list[i] in SentenceCheckUtils.__banned_words or word_list[i] not in SentenceCheckUtils.__model.vocab:
                continue

            sim = SentenceCheckUtils.__model.similarity(name, word_list[i])
            if sim > max:
                max = sim
            avg += sim

        avg /= len(word_list)

        data_f.write(f"{index},{max},{avg},{f}\n")


    @staticmethod
    def check(sentense, devices, sent_f):
        word_list = sentense.split()
        for device in devices:
            device.max = -10000
            device.avg = 0
            for device_name in device.get_names():
                avg = 0
                for i in range(len(word_list)):
                    if word_list[i] in SentenceCheckUtils.__banned_words or word_list[i] not in SentenceCheckUtils.__model.vocab:
                        continue

                    sim = SentenceCheckUtils.__model.similarity(device_name, word_list[i])
                    if sim > device.max:
                        device.max = sim
                    avg += sim

                avg /= len(word_list)
                if avg > device.avg:
                    device.avg = avg
            sent_f.write(str(device.max) + ";")
            sent_f.write(str(device.avg) + "\n")

        devices.sort(key=lambda x: SentenceCheckUtils.__coef[0] * x.max + SentenceCheckUtils.__coef[0] * x.avg, reverse=True)
        return devices[0]
