from functors import *
from check_utils import SentenceCheckUtils
from device import Device

SentenceCheckUtils.init_model('tmp/GoogleNews-vectors-negative300.bin')

coffee = Device(["coffee"], HttpSender("172.168.0.1", "coffee one"))

home = Device(["home"])
home.set_devices([coffee])

input_file = open('input/tests.txt', 'r')
sent_f = open('output/data.csv', 'a')
sent_f.write(",x1,x2,y\n")
i = 0
for sentense in input_file:
    try:
        print("sentense: ")
        print(sentense)
        split = sentense.split(";")
        print(split[0])
        SentenceCheckUtils.add_to_dataset(split[0], "coffee", sent_f, i, split[1])
        i = i + 1
    except Exception as err:
        print(str(err))
        print("input_error")



input_file.close()

# while(True):
#     sentense = input()
#     if (sentense == 'exit'):
#         break
#     try:
#         answer = home.check(sentense)
#         print(answer)
#         answer.function()
#     except Exception as errr:
#         print("input_error")



