import gensim
import home_object

print('Loading of the model started...')
model = gensim.models.KeyedVectors.load_word2vec_format('tmp/GoogleNews-vectors-negative300.bin', binary=True)
print('Model loaded...')

config_file = open('input/config', 'r')
home = home_object.device('home')

coffee = home_object.device('coffee')
coffee.set_final(True)

home.add_device(coffee)

sugar = home_object.device('sugar')

sugar.add_device(home_object.device('without'))
sugar.add_device(home_object.device('one'))
sugar.add_device(home_object.device('two'))
sugar.add_device(home_object.device('three'))

coffee.add_device(sugar)

input_file = open('input/new_tests.txt', 'r')
result_f = open('output/result.txt', 'a')
for sentense in input_file:
    answer = home.check(model, sentense, 'output/sent.txt', 'output/words.txt')
    result_f.write(f"********************\n{sentense}\n")
    result_f.write(f"{answer}\n")

input_file.close()
result_f.close()

# flag = False
# f = open('text.txt', 'a')
# while flag:
#     print('enter: ')
#     s = input()
#
#     if s == 'pidr':
#         f.close()
#         break
#
#     t = s.split()
#
#     for i in range(len(t) - 1):
#         for j in range(len(t) - 1):
#             try:
#                 sim = model.similarity(t[i], t[j])
#                 answer = t[i] + ' sim ' + t[j] + '->' + str(sim)
#                 print(answer)
#                 f.write(answer + '\n')
#             except Exception as err:
#                 print(err)
#
#f.close()
#model = gensim.models.Word2Vec.load_word2vec_format('./model/GoogleNews-vectors-negative300.bin', binary=True)

#sentences = [['first', 'sentence'], ['second', 'sentence']]
# train word2vec on the two sentences
#model = gensim.models.Word2Vec(sentences, min_count=1)

#print (model.similarity('first', 'sentence'))
#print (model.similarity('sentence', 'sentence'))
#print (model.similarity('first', 'second'))

#model = gensim.models.Word2Vec(iter=1)  # an empty model, no training yet
#model.build_vocab('tmp/train.txt')  # can be a non-repeatable, 1-pass generator
#model.train('tmp/train.txt')  # can be a non-repeatable, 1-pass generator

#model = gensim.models.Word2Vec('tmp/train.txt')
#print('train started...')
#model.accuracy('tmp/train.txt')
#print('train ended...')

#model.most_similar(positive=['woman', 'king'], negative=['man'], topn=1)
#[('queen', 0.50882536)]
#model.doesnt_match("breakfast cereal dinner lunch".split())
#'cereal'
#model.similarity('woman', 'man')
#0.73723527


