config_file = open('input/config', 'r')

for word in config_file:
    word_strip = word.strip()
    print(f"word '{word_strip}' is not in vocablurary")


config_file.close()

from nltk.corpus import stopwords
import nltk


stopWords = list(stopwords.words('english'))
print(stopWords)