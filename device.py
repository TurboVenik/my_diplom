from check_utils import SentenceCheckUtils
class Device:

    def __init__(self, names, function = None):
        self.__names = names
        self.__devices = None
        self.__parent = None
        self.function = function

    def get_names(self):
        return self.__names

    def set_parent(self, parent):
        self.__parent = parent

    def set_devices(self, devices):
        self.__devices=devices
        for device in devices:
            device.set_parent(self)

    def get_devices(self):
        return self.__devices

    def check(self, sentense, s):
        devices = self.__devices
        device = None
        while (devices != None):
            device = SentenceCheckUtils.check(sentense, devices, s)
            devices = device.get_devices()
        return device

    def __str__(self):
        return ("" if self.__parent is None else str(self.__parent)) + " " + str(self.__names[0])